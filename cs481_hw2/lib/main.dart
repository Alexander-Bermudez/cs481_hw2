import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'This is my PC', style: TextStyle(
                    fontWeight: FontWeight.bold,),
                  ),
                ),
                Text("It's so cute, right?", style: TextStyle(
                color: Colors.grey[500],),
                ),
              ],
            )
          ),
        FavoriteWidget(),
        ],
      ),
    );
    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          PCWidget(),
          GameWidget(),
          StarWidget(),
        ],
      ),
    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'This is my beautiful PC, and yes it is an absolute beast '
            'of a computer, because it has some awesome specs. It is '
            'currently rocking an AMD Ryzen 7 3700X, a MSI B550 Motherboard, '
            '16GB of GSkill Ripjaws RAM, and last but definitely not least: '
            'a GeForce RTX 2070 Super graphics card. This baby gets me through '
            'even the toughest of days, and can run anything I throw at it like '
            'its NOTHING. Lately, (other than classwork) Ive been using this to '
            'run games such as: Call of Duty, Hell Let Loose, Escape from Tarkov, '
            'Rainbow Six: Siege, and The Elder Scrolls V: Skyrim. My friend asked '
            'me once "Why a PC?", and I just said "Typical Console user."',
        softWrap: true,
      ),
    );

    return MaterialApp(
      title: 'Stateful HW: My PC',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Stateful HW: My PC'),
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/MYPC.jpg',
              width: 240,
              height: 600,
              fit: BoxFit.cover,
            ),
            titleSection,
            buttonSection,
            textSection
          ]
        ),
      ),
    );
  }
  Column _buildButtonColumn(Color color, IconData icon, String label){
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }

}

class FavoriteWidget extends StatefulWidget {
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool _isFavorited = true;
  int _favoriteCount = 23;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon:(_isFavorited ? Icon(Icons.favorite) : Icon(Icons.favorite_border)),
            color: Colors.red[500],
            onPressed: _toggleFavorite,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
            child: Text('$_favoriteCount'),
          ),
        ),
      ],
    );
  }
  void _toggleFavorite() {
    setState(() {
      if (_isFavorited) {
        _favoriteCount -= 1;
        _isFavorited = false;
      }
      else {
        _favoriteCount += 1;
        _isFavorited = true;
      }
    });
  }
}

class PCWidget extends StatefulWidget {
  @override
  _PCWidgetState createState() => _PCWidgetState();
}

class _PCWidgetState extends State<PCWidget> {
  bool _isSelected = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon:(_isSelected ? Icon(Icons.desktop_windows, color: Colors.blue) : Icon(Icons.whatshot)),
            color: Colors.red[500],
            onPressed: _toggleSelected,
          ),
        ),
        if (_isSelected) SizedBox(
          width: 80,
          child: Container(
            padding: EdgeInsets.all(0),
            child:
            Text('My PC',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.blue,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ) else SizedBox(
            width: 80,
            child: Container(
              child:
              Text('Its Pretty Fire',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Colors.red,
                ),
                textAlign: TextAlign.center,

              ),
            ),
        ),
      ],
    );
  }
  void _toggleSelected() {
    setState(() {
      if (_isSelected) {
        _isSelected = false;
      }
      else {
        _isSelected = true;
      }
    });
  }
}

class GameWidget extends StatefulWidget {
  @override
  _GameWidgetState createState() => _GameWidgetState();
}

class _GameWidgetState extends State<GameWidget> {
  bool _isSelected = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon:(_isSelected ? Icon(Icons.videogame_asset, color: Colors.blue) : Icon(Icons.done)),
            color: Colors.green,
            onPressed: _toggleSelected,
          ),
        ),
        if (_isSelected) SizedBox(
          width: 80,
          child: Container(
            padding: EdgeInsets.all(0),
            child:
            Text('Games',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.blue,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ) else SizedBox(
          width: 80,
          child: Container(
            child:
            Text('All of them!',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.green,
              ),
              textAlign: TextAlign.center,

            ),
          ),
        ),
      ],
    );
  }
  void _toggleSelected() {
    setState(() {
      if (_isSelected) {
        _isSelected = false;
      }
      else {
        _isSelected = true;
      }
    });
  }
}

class StarWidget extends StatefulWidget {
  @override
  _StarWidgetState createState() => _StarWidgetState();
}

class _StarWidgetState extends State<StarWidget> {
  bool _isSelected = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon:(_isSelected ? Icon(Icons.grade, color: Colors.blue) : Icon(Icons.stars)),
            color: Colors.orangeAccent,
            onPressed: _toggleSelected,
          ),
        ),
        if (_isSelected) SizedBox(
          width: 80,
          child: Container(
            padding: EdgeInsets.all(0),
            child:
            Text('Stars?',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.blue,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ) else SizedBox(
          width: 80,
          child: Container(
            child:
            Text('Five Stars!!',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.orangeAccent,
              ),
              textAlign: TextAlign.center,

            ),
          ),
        ),
      ],
    );
  }
  void _toggleSelected() {
    setState(() {
      if (_isSelected) {
        _isSelected = false;
      }
      else {
        _isSelected = true;
      }
    });
  }
}
