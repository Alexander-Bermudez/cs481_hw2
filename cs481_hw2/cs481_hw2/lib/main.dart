import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'This is my PC', style: TextStyle(
                    fontWeight: FontWeight.bold,),
                  ),
                ),
                Text("It's so cute, right?", style: TextStyle(
                color: Colors.grey[500],),
                ),
              ],
            )
          ),
        ],
      ),
    );
    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.computer, 'My PC'),
          _buildButtonColumn(color, Icons.videogame_asset, 'Games'),
          _buildButtonColumn(color, Icons.whatshot, 'Hot'),
        ],
      ),
    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'This is my beautiful PC, and yes it is an absolute beast '
            'of a computer, because it has some awesome specs. It is '
            'currently rocking an AMD Ryzen 7 3700X, a MSI B550 Motherboard, '
            '16GB of GSkill Ripjaws RAM, and last but definitely not least: '
            'a GeForce RTX 2070 Super graphics card. This baby gets me through '
            'even the toughest of days, and can run anything I throw at it like '
            'its NOTHING. Lately, (other than classwork) Ive been using this to '
            'run games such as: Call of Duty, Hell Let Loose, Escape from Tarkov, '
            'Rainbow Six: Siege, and The Elder Scrolls V: Skyrim. My friend asked '
            'me once "Why a PC?", and I just said "Typical Console user."',
        softWrap: true,
      ),
    );

    return MaterialApp(
      title: 'Layout Homework: My PC',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Layout Homework: My PC'),
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/MYPC.jpg',
              width: 240,
              height: 600,
              fit: BoxFit.cover,
            ),
            titleSection,
            buttonSection,
            textSection
          ]
        ),
      ),
    );
  }
  Column _buildButtonColumn(Color color, IconData icon, String label){
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }

}
